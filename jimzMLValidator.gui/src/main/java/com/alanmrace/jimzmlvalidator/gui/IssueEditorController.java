package com.alanmrace.jimzmlvalidator.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.alanmrace.jimzmlparser.event.CVParamAddRemoveEvent;
import com.alanmrace.jimzmlparser.event.CVParamAddedEvent;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import com.alanmrace.jimzmlparser.obo.OBOTerm.XMLType;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleWithMzMLContentException;
import com.alanmrace.jimzmlvalidator.exceptions.UncheckableMappingRuleException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import com.alanmrace.jimzmlparser.exceptions.Issue;
import com.alanmrace.jimzmlparser.exceptions.NonFatalParseException;
import com.alanmrace.jimzmlparser.event.MzMLContentListener;
import com.alanmrace.jimzmlparser.event.MzMLEvent;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.CVMapping;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;

/**
 * FXML Controller class
 *
 * @author Alan
 */
public class IssueEditorController implements Initializable {

    @FXML
    Label issueTitleLabel;

    @FXML
    Label issueMessageLabel;

    @FXML
    VBox mustCVBox;

    @FXML
    VBox shouldCVBox;

    @FXML
    VBox mayCVBox;

    protected ObservableList<Issue> issueList;

    protected HashMap<String, CVMapping> cvMapping;

    protected MzMLContentWithParams currentMzMLContent;
    protected MzMLContentListener currentMzMLContentListener;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
/*        try {
            // TODO
            ImzML imzML = ImzMLHandler.parseimzML("D:\\Alan\\2017-02-24_mouse_brain_DHB_10fliter_15att__LM716_mz500-1600_50um_235x155_Jasmin_afterDIGEST.RAW.imzML");
            TreeItem<MzMLTag> root = new TreeItem<>(imzML);

            HashMap<MzMLTag, TreeItem<MzMLTag>> treeItemView = ImzMLValidatorController.createTreeItemMap(imzML, root);

            MzMLTag tag = imzML.getSpectrumList().getSpectrum(0);
            tag = imzML.getScanSettingsList().getScanSettings(0);

            System.out.println(tag);

            TreeItem<MzMLTag> child = treeItemView.get(tag);
            System.out.println(child);

            String xPath = getXPathFromTreeItem(child);

            System.out.println();

            List<CVMapping> cvMappingList = ImzMLValidator.getDefaultMappingList();
            cvMapping = new HashMap<>();

            List<CVMappingRule> mappingRules = new LinkedList<>();

            for (CVMapping cvMap : cvMappingList) {
                for (CVMappingRule rule : cvMap.getCVMappingRuleList()) {
                    if (rule.getScopePath().equals(xPath)) {
                        mappingRules.add(rule);

                        updateContentListener((MzMLContentWithParams) tag);
                        
                        //updateRule(rule, (MzMLContentWithParams) tag);
                        generateRuleInterface(rule, (MzMLContentWithParams) tag);
                    }
                }
            }
        } catch (FatalParseException ex) {
            Logger.getLogger(IssueEditorController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    public static String getXPathFromTreeItem(TreeItem<MzMLTag> child) {
        if (child.getParent() == null) {
            return "/" + child.getValue().getTagName();
        }

        return getXPathFromTreeItem(child.getParent()) + "/" + child.getValue().getTagName();
    }

    public void intitialiseIssueList(ObservableList<Issue> issueList) {
        this.issueList = issueList;
//        System.out.println(issueList);
    }

    public void editTag(MzMLTag tag) {

    }

    public void selectIssue(int issueIndex) {
        if (issueIndex >= 0) {
//            System.out.println(issueList.get(issueIndex));
            Issue issue = issueList.get(issueIndex);

//            issueTitleLabel.setText(issue.getIssueTitle());
//            issueMessageLabel.setText(issue.getIssueMessage());

            mustCVBox.getChildren().clear();
            shouldCVBox.getChildren().clear();
            mayCVBox.getChildren().clear();

            if (issue instanceof UncheckableMappingRuleException) {

            } else if (issue instanceof CVMappingRuleWithMzMLContentException) {
                CVMappingRule rule = ((CVMappingRuleWithMzMLContentException) issue).getCVMappingRule();
                MzMLContentWithParams mzMLContent = ((CVMappingRuleWithMzMLContentException) issue).getMzMLContent();

                updateContentListener(mzMLContent);
                
                //updateRule(rule, mzMLContent);
                generateRuleInterface(rule, mzMLContent);
            }
        } else {
            // TODO: CLEAR??
        }
    }

    protected HashMap<CVTerm, VBox> termBoxMap = new HashMap<>();
    protected HashMap<CVTerm, Label> ruleTermOKMap = new HashMap<>();

    protected void generateRuleInterface(CVMappingRule rule, MzMLContentWithParams mzMLContent) {
        VBox cvBox;

        switch (rule.getRequirementLevel()) {
            case MUST:
                cvBox = mustCVBox;
                break;
            case SHOULD:
                cvBox = shouldCVBox;
                break;
            case MAY:
            default:
                cvBox = mayCVBox;
                break;
        }

        for (CVTerm term : rule.getCVTerms()) {
            VBox vBox = new VBox();

            termBoxMap.put(term, vBox);

            HBox hBox = new HBox();
            vBox.getChildren().add(hBox);

            Label termOKLabel = new Label();
            hBox.getChildren().add(termOKLabel);

            ruleTermOKMap.put(term, termOKLabel);

            Label label = new Label();
            label.setText(term.getDescription());
            hBox.getChildren().add(label);

            String oboTerm = term.getTermAccession();

            List<CVParam> params = new LinkedList<>();

            if (term.canUseTerm()) {
                params.add(mzMLContent.getCVParam(oboTerm));
            }
            if (term.canUseChildren()) {
                params.addAll(mzMLContent.getChildrenOf(oboTerm, false));
            }

            for (CVParam param : params) {
                addTermSelectionBox(mzMLContent, term, param);
            }

            // If there is no 
            if (term.isRepeatable() || params.isEmpty()) {
                addTermSelectionBox(mzMLContent, term, null);
            }

            cvBox.getChildren().add(vBox);
        }
    }

    protected void removeLastTermSelectionBox(CVTerm term) {
        VBox vBox = termBoxMap.get(term);
        
        vBox.getChildren().remove(vBox.getChildren().size()-1);
    }
    
    protected void resetTermSelectionBox(MzMLContentWithParams mzMLContent, CVTerm term, CVParam param) {
        removeLastTermSelectionBox(term);
        addTermSelectionBox(mzMLContent, term, param);
    }
    
    protected void addTermSelectionBox(MzMLContentWithParams mzMLContent, CVTerm term, CVParam param) {
        VBox vBox = termBoxMap.get(term);

        HBox comboFillInBox = new HBox();
        vBox.getChildren().add(comboFillInBox);
        ComboBox cBox = new ComboBox();
        comboFillInBox.getChildren().add(cBox);

        HBox fillInBox = new HBox();
        comboFillInBox.getChildren().add(fillInBox);

        OBOTerm parent = OBO.getOBO().getTerm(term.getTermAccession());

        if (term.canUseTerm()) {
            cBox.getItems().add(parent);
        }

        if (term.canUseChildren()) {
            cBox.getItems().addAll(parent.getAllChildren(false));
        }

        //if (cBox.getItems().size() == 1) {
        //    cBox.getSelectionModel().select(0);
        //}

        if (param != null) {
            // Select the CVParam that was included within the 
            cBox.getSelectionModel().select(param.getTerm());

            showValueAndUnits(fillInBox, term, param.getTerm(), param);
        }

        updateTermCheck(mzMLContent, term);
        
        // The following will only work with rules that are 'one time only' rules.
        // If the rule has 'one or more' then it should not be a selection box
        cBox.getSelectionModel().selectedItemProperty().addListener((listener, oldValue, newValue) -> {
            OBOTerm newTerm = (OBOTerm) newValue;
            
            // If the term is repeatable, add another selection box
            if (oldValue == null && newValue != null && term.isRepeatable()) {
                addTermSelectionBox(mzMLContent, term, null);
            }

            CVParam cvParam;
            try {
                cvParam = CVParam.createCVParam(newTerm, null);

                showValueAndUnits(fillInBox, term, newTerm, cvParam);

                // TODO: Remove old CVParam and add in new one
                if (oldValue != null) {
                    mzMLContent.removeCVParam(((OBOTerm) oldValue).getID());
                }
                mzMLContent.addCVParam(cvParam);

                updateTermCheck(mzMLContent, term);
                
                // Check all other terms to see if they need updating
                for(CVTerm currentTerm : termBoxMap.keySet()) {
                    if(!currentTerm.equals(term) && currentTerm.isOBOTermCovered(newTerm)) {
                        if(currentTerm.isRepeatable()) {
                            removeLastTermSelectionBox(currentTerm);
                            addTermSelectionBox(mzMLContent, currentTerm, cvParam);
                            addTermSelectionBox(mzMLContent, currentTerm, null);
                        }
                        
                        updateTermCheck(mzMLContent, currentTerm);
                    }
                }
            } catch (NonFatalParseException ex) {
                Logger.getLogger(IssueEditorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    
    protected void updateTermCheck(MzMLContentWithParams mzMLContent, CVTerm term) {
        Label termOKLabel = ruleTermOKMap.get(term);

        try {
            boolean termOK = term.check(mzMLContent);
            
            termOKLabel.setText(termOK ? "✓" : "✖");
        } catch (CVMappingRuleException ex) {
        }
    }

    protected void updateContentListener(MzMLContentWithParams mzMLContent) {
        // Remove the listener from the previously edited content as it is no longer
        // required
        if (currentMzMLContent != null) {
            currentMzMLContent.removeListener(currentMzMLContentListener);
        }

        // Store details of the current content and listener so that it can 
        // be removed later
        currentMzMLContent = mzMLContent;
        currentMzMLContentListener = (MzMLEvent event) -> {
            // Depending on event type we can do something..
            
//            System.out.println("Event occured " + event);
            
            if(event instanceof CVParamAddRemoveEvent) {
                CVParamAddRemoveEvent addRemoveEvent = (CVParamAddRemoveEvent) event;
                CVParam param = addRemoveEvent.getCVParam();
                
                for(CVTerm term : termBoxMap.keySet()) {
                    try {
                        if(term.check(param)) {
                            if(!term.isRepeatable()) {
                                if(addRemoveEvent instanceof CVParamAddedEvent)
                                    resetTermSelectionBox(addRemoveEvent.getSource(), term, addRemoveEvent.getCVParam());
                                else
                                    resetTermSelectionBox(addRemoveEvent.getSource(), term, null);
                            }

                            updateTermCheck(addRemoveEvent.getSource(), term);
                        }
                    } catch (CVMappingRuleException ex) {
                        Logger.getLogger(IssueEditorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };

        mzMLContent.addListener(currentMzMLContentListener);
    }
/*
    protected void updateRule(CVMappingRule rule, MzMLContentWithParams mzMLContent) {

        VBox cvBox;

        switch (rule.getRequirementLevel()) {
            case MUST:
                cvBox = mustCVBox;
                break;
            case SHOULD:
                cvBox = shouldCVBox;
                break;
            case MAY:
            default:
                cvBox = mayCVBox;
                break;
        }

//        Label contentLabel = new Label();
//        contentLabel.setText(mzMLContent.toString());
//        cvBox.getChildren().add(contentLabel);
        for (CVTerm term : rule.getCVTerms()) {
            boolean termOK = false;

            try {
                termOK = term.check(mzMLContent);
            } catch (CVMappingRuleException ex) {
                // Don't do anything - just checking if there are any exceptions
                System.out.println(ex);
            }

            VBox vBox = new VBox();
            HBox hBox = new HBox();
            vBox.getChildren().add(hBox);

            Label termOKLabel = new Label();
            termOKLabel.setText(termOK ? "✓" : "✖");
            hBox.getChildren().add(termOKLabel);

            Label label = new Label();
            label.setText(term.getDescription());
            hBox.getChildren().add(label);

            HBox comboFillInBox = new HBox();
            vBox.getChildren().add(comboFillInBox);
            ComboBox cBox = new ComboBox();
            comboFillInBox.getChildren().add(cBox);

            HBox fillInBox = new HBox();
            comboFillInBox.getChildren().add(fillInBox);

            OBOTerm parent = OBO.getOBO().getTerm(term.getTermAccession());

            if (term.canUseTerm()) {
                cBox.getItems().add(parent);
            }

            if (term.canUseChildren()) {
                cBox.getItems().addAll(parent.getAllChildren());
            }

            if (cBox.getItems().size() == 1) {
                cBox.getSelectionModel().select(0);
            }

            // If the term was OK then fill in the details
            if (termOK) {
                // Select the CVParam that was included within the 
                for (Object curOBOTerm : cBox.getItems()) {
                    CVParam param = mzMLContent.getCVParam(((OBOTerm) curOBOTerm).getID());

                    if (param != null) {
                        cBox.getSelectionModel().select(curOBOTerm);

                        showValueAndUnits(fillInBox, term, param.getTerm(), param);

                        break;
                    }
                }
            }

            // The following will only work with rules that are 'one time only' rules.
            // If the rule has 'one or more' then it should not be a selection box
            cBox.getSelectionModel().selectedItemProperty().addListener((listener, oldValue, newValue) -> {
                OBOTerm newTerm = (OBOTerm) newValue;

                CVParam param;
                try {
                    param = CVParam.createCVParam(newTerm, null);

                    showValueAndUnits(fillInBox, term, newTerm, param);

                    if (oldValue != null) {
                        System.out.println("Before:" + mzMLContent.getCVParam(((OBOTerm) oldValue).getID()));
                    } else {
                        System.out.println("Before: null");
                    }

                    // TODO: Remove old CVParam and add in new one
                    if (oldValue != null) {
                        mzMLContent.removeCVParam(((OBOTerm) oldValue).getID());
                    }
                    mzMLContent.addCVParam(param);

                    if (newValue != null) {
                        System.out.println("After:" + mzMLContent.getCVParam(((OBOTerm) newValue).getID()));
                    } else {
                        System.out.println("After: null");
                    }
                } catch (NonFatalParseException ex) {
                    Logger.getLogger(IssueEditorController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            cvBox.getChildren().add(vBox);
        }
    }
*/

//    protected void showValueAndUnits(HBox fillInBox, OBOTerm newTerm) {
//        showValueAndUnits(fillInBox, newTerm, null);
//    }
    protected void showValueAndUnits(HBox fillInBox, CVTerm term, OBOTerm newTerm, CVParam param) {
        XMLType type = newTerm.getValueType();

        fillInBox.getChildren().clear();

        if (type != null) {
            // Add in value box
            TextField valueField = new TextField();
            valueField.setText(param.getValueAsString());

            param.addListener((content) -> {
                valueField.setText(param.getValueAsString());
            });
            
            valueField.textProperty().addListener((observable, oldValue, newValue) -> {
                // Check if a change has actually been made, and only then change the 
                // value to avoid needlessly calling the notifyListeners().
                if (!param.getValueAsString().equals(newValue)) {
                    try {
                        param.setValueAsString(newValue);
//                        System.out.println(param + " changed from " + oldValue + " to " + newValue);
                    } catch (Exception ex) {
//                        System.out.println(param + " failed to change from " + oldValue + " to " + newValue);
                        valueField.setText(oldValue);
                    }
                }
            });

            fillInBox.getChildren().add(valueField);

            if (!newTerm.getUnits().isEmpty()) {
                ComboBox cBox = new ComboBox();
                fillInBox.getChildren().add(cBox);
                cBox.getItems().addAll(newTerm.getUnits());

                if (cBox.getItems().size() == 1) {
                    cBox.getSelectionModel().select(0);
                }
            }
        }
        
        // If the term is repeatable then we can add a delete button
        if (term.isRepeatable()) {
            // Add option to delete the CVParam
            Button deleteButton = new Button();
            deleteButton.setText("X");

            deleteButton.setOnAction((event) -> {
                // TODO: Delete
                System.out.println("Delete: " + newTerm + " from " + currentMzMLContent);
                
                currentMzMLContent.removeCVParam(newTerm.getID());
//                System.out.println(deleteButton.getParent());
//                System.out.println(deleteButton.getParent().getParent());
                
                HBox hbox1 = (HBox) deleteButton.getParent();
                HBox hbox2 = (HBox) deleteButton.getParent().getParent();
                
                hbox2.getChildren().clear();
                //ComboBox comboBox = (ComboBox) hbox2.getChildren().get(0);
//                System.out.println(Arrays.toString(hbox2.getChildren().toArray()));
                
                updateTermCheck(currentMzMLContent, term);
            });

            fillInBox.getChildren().add(deleteButton);
        }
    }
}
