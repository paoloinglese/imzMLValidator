/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.gui;

import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlparser.obo.OBOTerm;
import com.alanmrace.jimzmlvalidator.CVTerm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 *
 * @author alan.race
 */
public class CVParamEditor extends HBox {

    @FXML
    ComboBox oboTermBox;

    @FXML
    TextField valueTextField;

    @FXML
    ComboBox unitsBox;

    CVTerm cvTerm;
    MzMLContentWithParams mzMLContent;
//    CVParam cvParam;

    CVParamModel paramModel;

    public CVParamEditor() {
        super();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CVParamEditor.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        new AutoCompleteComboBoxListener<>(oboTermBox);
        valueTextField.managedProperty().bind(valueTextField.visibleProperty());
        unitsBox.managedProperty().bind(unitsBox.visibleProperty());

        // Add listeners after initial values are set
//        createListeners();
    }

    public void initialise(CVTerm cvTerm, MzMLContentWithParams mzMLContent, CVParam cvParam) {
        this.cvTerm = cvTerm;
        this.mzMLContent = mzMLContent;

        paramModel = new CVParamModel(cvParam);
        
//        oboTermBox.valueProperty().bindBidirectional(paramModel.getOBOTermProperty());
        valueTextField.textProperty().bindBidirectional(paramModel.getValueProperty());
        unitsBox.valueProperty().bindBidirectional(paramModel.getUnitsProperty());

        valueTextField.visibleProperty().bind(paramModel.getHasValueProperty());
        unitsBox.visibleProperty().bind(paramModel.getHasUnitsProperty());

        oboTermBox.setCellFactory(param -> {
            ListCell<OBOTerm> cell = new ListCell<OBOTerm>() {
                @Override
                public void updateItem(OBOTerm item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item != null) {
                        setText(item.toString()); //item.getName());

                        this.setTooltip(new Tooltip(item.getDescription()));
                    } else {
                        setText(null);
                        setTooltip(null);
                    }
                }
            };
            
            cell.setOnMousePressed(e -> {
//                System.out.println(cell.getItem());
                
                paramModel.getOBOTermProperty().setValue(cell.getItem());
            });
            
            return cell;
        });
        
        // By default reset the style of the value box
        paramModel.getValueProperty().addListener((event) -> {
            valueTextField.setStyle(null);
        });
        
        // But if there is an exception caused in the param model (can check if this
        // is related to NumberFormatException if necessary) then change text to red
        // to indicate the error
        paramModel.addListener((exception) -> {
            valueTextField.setStyle("-fx-text-fill: red;");
        });
        
        paramModel.getCVParamProperty().addListener((property, oldValue, newValue) -> {
            mzMLContent.removeCVParam(oldValue);

            boolean containsCVParam = false;

            // Have to use explict comparison check because equals is overriden
            List<CVParam> cvParamList = mzMLContent.getCVParamList();
            
            if(cvParamList != null) {
                for (CVParam childParam : cvParamList) {
                    if (childParam == newValue) {
                        containsCVParam = true;
                    }
                }
            }

            if (!containsCVParam) {
                mzMLContent.addCVParam(newValue);
            }

            updateUnits();
        });

        // Update with initial values
        update();

        // Add in the change listener 
//        oboTermBox. .setOnMouseClicked((event) -> {
//            System.out.println("HELLO");
//        });
//        oboTermBox.valueProperty().addListener((event) -> {
//            System.out.println(event);
//            
//            if(event instanceof ObjectProperty) {
//                Object bean = ((ObjectProperty) event).getBean();
//                
//                if(bean instanceof ComboBox) {
//                    Object value = ((ComboBox) bean).getValue();
//                    
//                    String newVal = ((ComboBox) bean).getEditor().getText();
//                    System.out.println(newVal);
//                    
//                    if(value instanceof OBOTerm) {
//                        System.out.println("OBOTerm");
//
//                        System.out.println(value.toString());
//
//                        if(!newVal.equals(value.toString())) {
//                            System.out.println("NOT EQUAL!");
//                        }
//                    } else {
//                        paramModel.getOBOTermProperty().set(null);
//                        ((ComboBox) bean).getEditor().setText("");
//                    }
//                }
//            }
//            
//            System.out.println();
//        });
    }

    public CVParam getCVParam() {
        return paramModel.getCVParamProperty().get();
    }

    public void setCVParam(CVParam cvParam) {
        paramModel.getCVParamProperty().set(cvParam);
    }

    protected ComboBox getOBOComboBox() {
        return oboTermBox;
    }

    private void createListeners() {
//        System.out.println("Creating listeners for OBOTermBox hash: " + oboTermBox.hashCode() +" (" + oboTermBox.getSelectionModel().selectedItemProperty().hashCode() + ")");

        /*        oboTermBox.getSelectionModel().selectedItemProperty().addListener((listener, oldValue, newValue) -> {
            
            System.out.println("Changing " + oldValue + " to " + newValue);
            System.out.println("Changing " + ((oldValue == null)? null : oldValue.getClass()) + " to " + ((newValue == null)? null : newValue.getClass()));
            
            if(newValue instanceof String) // || oldValue instanceof String)
                return;
                       
            // Only continue if the value has been changed and it is a valid value
            if(newValue != null && !newValue.equals(oldValue)) {
                System.out.println("CVParamEditor#createListeners(): " + newValue.getClass() + ": " + newValue);
                
                OBOTerm newTerm = (OBOTerm) newValue;

                // If the new term is equal to that of the term stored in the 
                // cvParam then it is likely that the selected value was changed
                // programmatically, and in those cases we do not want to add
                // a new CVParam to the MzMLContent
                if(cvParam == null || !cvParam.getTerm().equals(newTerm)) {                
                    try {
                        if (oldValue != null) {
                            this.mzMLContent.removeCVParam(this.cvParam);
                        }
                        
                        // New value has been selected so create an appropriate
                        // CVParam and add it to the MzMLContent
                        this.cvParam = CVParam.createCVParam(newTerm, null);


                        if(!mzMLContent.getCVParamList().contains(cvParam)) {
                            this.mzMLContent.addCVParam(cvParam);

                            updateCVParam();
                        }
                    } catch (NonFatalParseException ex) {
                        Logger.getLogger(IssueEditorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });*/
 /*        valueTextField.textProperty().addListener((observable, oldValue, newValue) -> {
                // Check if a change has actually been made, and only then change the 
                // value to avoid needlessly calling the notifyListeners().
                if (!cvParam.getValueAsString().equals(newValue)) {
                    try {
                        cvParam.setValueAsString(newValue);
                    } catch (Exception ex) {
                        valueTextField.setText(oldValue);
                    }
                }
            });
         */
        // TODO: Units combo listener
    }

//    protected void updateCVParam() {
//        valueTextField.setVisible(false);
//        unitsBox.setVisible(false);
//        if (cvParam != null && !(cvParam instanceof EmptyCVParam)) {
//            valueTextField.setVisible(true);
//            valueTextField.setText(cvParam.getValueAsString());
//            cvParam.addListener((content) -> {
//                valueTextField.setText(cvParam.getValueAsString());
//            });            
//            List<OBOTerm> units = cvParam.getTerm().getUnits();
//            if (units != null && units.size() > 0) {
//                unitsBox.setVisible(true);
//                System.out.println("CVParamEditor#updateCVParam(): " + cvParam.getUnits());
//                unitsBox.getItems().addAll(units);
//                unitsBox.getSelectionModel().select(cvParam.getUnits());
//            }
//        }
//    }
    private void updateUnits() {
        // Update units for the new CVParam
        unitsBox.getItems().clear();

        if (getCVParam() == null) {
            return;
        }

        List<OBOTerm> units = getCVParam().getTerm().getUnits();

        if (units != null && units.size() > 0) {
            unitsBox.getItems().addAll(units);

            if (getCVParam().getUnits() != null) {
                unitsBox.getSelectionModel().select(getCVParam().getUnits());
            }
        }
    }

    static List<OBOTerm> fullTermList = new ArrayList<OBOTerm>();
    
    protected void update() {
        oboTermBox.getItems().clear();

        if (cvTerm != null) {
            Set<OBOTerm> oboTermList = cvTerm.getAllowedOBOTerms();

            oboTermBox.getItems().addAll(oboTermList);
        } else {
            if(fullTermList.isEmpty()) {
                OBO obo = OBO.getOBO();
                List<OBO> imports = obo.getImports();
            
                fullTermList.addAll(obo.getTerms());
                
                imports.forEach((importOBO) -> fullTermList.addAll(importOBO.getTerms()));
            }
            
            oboTermBox.getItems().addAll(fullTermList);
        }

        if (getCVParam() != null) {
            oboTermBox.getSelectionModel().select(getCVParam().getTerm());
        }

        updateUnits();
    }

}
