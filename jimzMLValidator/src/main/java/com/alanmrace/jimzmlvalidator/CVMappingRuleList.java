package com.alanmrace.jimzmlvalidator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Alan Race
 */
public class CVMappingRuleList implements Iterable<CVMappingRule> {

    private List<CVMappingRule> mappingRuleList;

    public CVMappingRuleList() {
    	mappingRuleList = new LinkedList<>();
    }

    public void addCVMappingRule(CVMappingRule mappingRule) {
        this.mappingRuleList.add(mappingRule);
    }
    
    public boolean removeCVMappingRule(CVMappingRule mappingRule) {
        return this.mappingRuleList.remove(mappingRule);
    }
    
    @Override
    public Iterator<CVMappingRule> iterator() {
        return mappingRuleList.iterator();
    }
    
    public CVMappingRule getCVMappingRule(String id) {
        for(CVMappingRule rule : mappingRuleList) {
            if(rule.getID().equals(id))
                return rule;
        }
        
        return null;
    }
}
