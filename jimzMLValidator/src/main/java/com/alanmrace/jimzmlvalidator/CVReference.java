package com.alanmrace.jimzmlvalidator;

import java.io.Serializable;

/**
 * Reference to a controlled vocabulary.
 * 
 * @author Alan Race
 */
public class CVReference implements Serializable {

    /**
     * Short label for the CV or namespace, this should correspond to a cvIdentifier attribute of CvTerm in the CvSourceList configuration file.
     */
    protected String cvName;
    
    /**
     * Full descriptive name for the CV.
     */
    protected String cvIdentifier;
    
    /**
     * Create a new reference to a controlled vocabulary (CV). 
     * 
     * @param cvName Full descriptive name for the CV.
     * @param cvIdentifier Short label for the CV or namespace, this should correspond to a cvIdentifier attribute of CvTerm in the CvSourceList configuration file.
     */
    public CVReference(String cvName, String cvIdentifier) {
        if(cvName == null)
            throw new IllegalArgumentException("Must supply a valid cvName");
        if(cvIdentifier == null)
            throw new IllegalArgumentException("Must supply a valid cvIdentifier");
        
        this.cvName = cvName;
        this.cvIdentifier = cvIdentifier;
    }
    
    /**
     * Create a shallow copy of a reference to a controlled vocabulary (CV). 
     * 
     * @param oldRef CV reference to copy.
     */
    public CVReference(CVReference oldRef) {
        this(oldRef.cvName, oldRef.cvIdentifier);
    }
    
    /**
     * Returns the identifier for the CV reference.
     * 
     * @return Identifier as a string.
     */
    public String getIdentifier() {
        return cvIdentifier;
    }
    
}
