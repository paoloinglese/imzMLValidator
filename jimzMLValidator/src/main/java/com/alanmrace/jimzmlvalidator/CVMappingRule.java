package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.exceptions.CVMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.CombinationMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidCVMappingException;
import com.alanmrace.jimzmlvalidator.exceptions.RepeatedMappingRuleException;
import com.alanmrace.jimzmlvalidator.exceptions.UnexpectedCVParamMappingRuleException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Representation of a controlled vocabulary mapping rule. 
 *
 * @author Alan Race
 */
public class CVMappingRule implements Serializable {

    /**
     * Unique serialisation ID.
     */
    private static final long serialVersionUID = 640112074898597333L;

    /**
     * Level at which the rule should be considered.
     */
    public enum RequirementLevel {

        /**
         * The given CV must be included within the file to be fully conforming to the specification (MUST).
         */
        MUST,

        /**
         * The given CV should be included however there may exist reasons why these can be ignored but their omission and any subsequent implications of their omission have been considered thoroughly (SHOULD).
         */
        SHOULD,

        /**
         * The given CV may or may not be included at the discretion of the user (MAY).
         */
        MAY
    }

    /**
     * Boolean logic, used to combine one or more CV rules.
     */
    public enum CombinationLogic {

        /**
         * Logical AND.
         */
        AND,

        /**
         * Logical OR.
         */
        OR,

        /**
         * Logical XOR.
         */
        XOR
    }

    /**
     * Convert a <code>String</code> to a <code>RequirementLevel</code>, when the <code>String</code> is equal to the exact name of a <code>RequirementLevel</code>.
     *
     * @param requirementLevel Name of the <code>RequirementLevel</code>.
     * @return <code>RequirementLevel</code> which has the name <code>requirementLevel</code>.
     * @throws InvalidCVMappingException Thrown when the <code>requirementLevel</code> argument doesn't match any <code>RequirementLevel</code>.
     */
    public static RequirementLevel stringToRequirementLevel(String requirementLevel) throws InvalidCVMappingException {
        switch (requirementLevel) {
            case "MUST":
                return RequirementLevel.MUST;
            case "SHOULD":
                return RequirementLevel.SHOULD;
            case "MAY":
                return RequirementLevel.MAY;
            default:
                throw new InvalidCVMappingException("Invalid requirement level " + requirementLevel);
        }
    }

    /**
     * Convert a <code>String</code> to a <code>CombinationLogic</code>, when the <code>String</code> is equal to the exact name of a <code>CombinationLogic</code>.
     *
     * @param combinationLogic Name of the <code>CombinationLogic</code>.
     * @return <code>CombinationLogic</code> which has the name <code>combinationLogic</code>.
     * @throws InvalidCVMappingException Thrown when the <code>combinationLogic</code> argument doesn't match any <code>CombinationLogic</code>.
     */
    public static CombinationLogic stringToCombinationLogic(String combinationLogic) throws InvalidCVMappingException {
        switch (combinationLogic) {
            case "AND":
                return CombinationLogic.AND;
            case "OR":
                return CombinationLogic.OR;
            case "XOR":
                return CombinationLogic.XOR;
            default:
                throw new InvalidCVMappingException("Invalid combination logic " + combinationLogic);
        }
    }

    /**
     * Unique identifier for this rule in the scope of the current configuration file. Identifiers are alphanumerical.
     */
    protected String id;

    /**
     * The full XPath expression that define the part of the data model we are mapping.
     */
    protected String cvElementPath;

    /**
     * The requirement level indicated, when the XML element exists in the instance data file, if the association with CV terms is optional (MAY), recommended (SHOULD) or mandatory (MUST).
     */
    protected RequirementLevel requirementLevel;

    /**
     * Element scope in the schema within which the non repeatable (isRepeatable = FALSE) condition applies.
     */
    protected String scopePath;

    /**
     * Boolean operator describing the combination logic of multiple CvTerm elements associated with the same CvMappingRule.
     */
    protected CombinationLogic combinationLogic;

    /**
     *  A short name for this rule. This may be used for error reporting.
     */
    protected String name;

    /**
     * List of CV terms that make up the mapping rule.
     */
    protected List<CVTerm> cvTerms;

    /**
     * Map of ignored CV terms when considering this mapping rule.
     */
    protected Map<CVTerm, Boolean> ignoreMap;

    /**
     * Ontology used to find CV terms and their children.
     */
    protected OBO obo;

    /**
     * Mapping rule description for a give scope within (i)mzML.
     *
     * @param id Unique identifier for the rule.
     * @param cvElementPath XPath of the CVParam element.
     * @param requirementLevel RequirementLevel of the rule.
     * @param scopePath XPath of the scope of the rule.
     * @param combinationLogic CombinationLogic of the rule.
     *
     * @see RequirementLevel
     * @see CombinationLogic
     */
    public CVMappingRule(String id, String cvElementPath, RequirementLevel requirementLevel,
            String scopePath, CombinationLogic combinationLogic) {
        this.id = id;
        this.cvElementPath = cvElementPath;
        this.requirementLevel = requirementLevel;
        this.scopePath = scopePath;
        this.combinationLogic = combinationLogic;

        this.cvTerms = new ArrayList<>();
        this.ignoreMap = new HashMap<>();
    }

    /**
     * Create a semi-deep copy (deep copy of the CV terms, but shallow copy of the ontology).
     * 
     * @param oldRule Rule to copy.
     */
    public CVMappingRule(CVMappingRule oldRule) {
        this(oldRule.id, oldRule.cvElementPath, oldRule.requirementLevel,
                oldRule.scopePath, oldRule.combinationLogic);

        oldRule.cvTerms.forEach(term -> cvTerms.add(new CVTerm(term)));

        this.obo = oldRule.obo;
        this.name = oldRule.name;
    }

    /**
     * Set the ontology.
     * 
     * @param obo Ontology.
     */
    public void setOBO(OBO obo) {
        this.obo = obo;
    }

    /**
     * Get the ontology.
     * 
     * @return Ontology.
     */
    public OBO getOBO() {
        return obo;
    }

    /**
     * Get the unique ID for this mapping rule.
     * 
     * @return ID.
     */
    public String getID() {
        return id;
    }

    /**
     * Get the short rule name.
     * 
     * @return Name for the rule.
     */
    public String getName() {
        if (name != null) {
            return name;
        }

        return id;
    }

    /**
     * Get the full XPath expression that defines the part of the model the rule applies to.
     * 
     * @return xPath expression.
     */
    public String getCVElementPath() {
        return cvElementPath;
    }

    /**
     * 
     * 
     * @return
     */
    public RequirementLevel getRequirementLevel() {
        return requirementLevel;
    }

    public String getScopePath() {
        return scopePath;
    }

    public CombinationLogic getCombinationLogic() {
        return combinationLogic;
    }

    public List<CVTerm> getCVTerms() {
        return cvTerms;
    }

    public void addCVTerm(CVTerm term) {
        if (obo != null) {
            term.setOBOTerm(obo.getTerm(term.getTermAccession()));
        }

        cvTerms.add(term);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        StringBuilder description = new StringBuilder("Rule [");

        if (name != null) {
            description.append(name);
        } else {
            description.append(id);
        }

        description.append("]\n");

        switch (requirementLevel) {
            case MUST:
                description.append("MUST");
                break;
            case SHOULD:
                description.append("SHOULD");
                break;
            case MAY:
                description.append("MAY");
                break;
        }

        description.append(" include ");

        switch (combinationLogic) {
            case AND:
                description.append("ALL");
                break;
            case OR:
                description.append("ANY");
                break;
            case XOR:
                description.append("ONE");
                break;
        }

        description.append(" of the following at ");
        description.append(this.cvElementPath);
        description.append("\n");

        cvTerms.forEach(cvTerm -> {
            description.append("\t");
            description.append(cvTerm.getDescription());
            description.append("\n");
        });

        return description.toString();
    }

    public void ignoreCVTerm(CVTerm termToIgnore) {
        ignoreCVTerm(termToIgnore, true);
    }

    public void ignoreCVTerm(CVTerm termToIgnore, boolean ignore) {
        this.ignoreMap.put(termToIgnore, ignore);
    }

    public boolean isIgnoring(CVTerm term) {
        if (ignoreMap.containsKey(term)) {
            return ignoreMap.get(term);
        }

        return false;
    }

    public void clearIgnore() {
        ignoreMap.clear();
    }

    private boolean[] determineIfTermsArePresent(MzMLContentWithParams contentToCheck) throws CVMappingRuleException {
        List<CVParam> cvParamList = contentToCheck.getCVParamList();

        boolean[] foundTerm = new boolean[cvParamList.size()];
        int[] termCount = new int[cvTerms.size()];

        for (int j = 0; j < cvParamList.size(); j++) {
            CVParam toCheck = cvParamList.get(j);
            foundTerm[j] = false;

            for (int i = 0; i < cvTerms.size(); i++) {
                CVTerm termToCheck = cvTerms.get(i);

                boolean termOK = termToCheck.check(toCheck, contentToCheck, this);

                foundTerm[j] |= termOK;

                if (termOK) {
                    termCount[i]++;

                    if (termCount[i] > 1 && !termToCheck.isRepeatable) {
                        throw new RepeatedMappingRuleException(this, contentToCheck, termToCheck);
                    }
                }
            }
        }
        
        return foundTerm;
    }
    
    private boolean checkOptionalSingle(MzMLContentWithParams contentToCheck) throws CVMappingRuleException {
        boolean isCorrect = false;

        // In the specific case of having a MAY rule set up with all OR values
        // check that only the members of the rule occur

        List<CVParam> cvParamList = contentToCheck.getCVParamList();

        if (cvParamList != null) {
            boolean[] foundTerm = determineIfTermsArePresent(contentToCheck);

            List<CVParam> notFoundTerms = new LinkedList<>();

            for (int j = 0; j < cvParamList.size(); j++) {
                if (!foundTerm[j]) {
                    notFoundTerms.add(cvParamList.get(j));
                }
            }

            isCorrect = notFoundTerms.isEmpty();

            if (!isCorrect) {
                throw new UnexpectedCVParamMappingRuleException(this, contentToCheck, notFoundTerms);
            }
        }

        return isCorrect;
    }
    
    private boolean checkRequiredOrMultiple(MzMLContentWithParams contentToCheck) throws CVMappingRuleException {
        boolean isCorrect = false;

        if (combinationLogic.equals(CombinationLogic.AND)) {
            isCorrect = true;
        }

        for (CVTerm term : cvTerms) {
            boolean termOK;

            if (ignoreMap.containsKey(term) && ignoreMap.get(term)) {
                termOK = true;
            } else {
                termOK = term.check(contentToCheck, this);
            }

            switch (combinationLogic) {
                case AND:
                    isCorrect &= termOK;
                    break;
                case OR:
                    isCorrect |= termOK;
                    break;
                case XOR:
                    isCorrect ^= termOK;
                    break;
                default:
                    break;
            }
        }

        if (!isCorrect) {
            throw new CombinationMappingRuleException(this, contentToCheck);
        }

        return isCorrect;
    }
    
    /**
     * Check that the supplied MzMLContent is valid according to the
     * CVMappingRule.
     * <p>
     * Checks are performed with the RequirementLevels matching that of RFC 2119
     * This means that if the requirement level is MAY and the combination is OR
     * (which it always should be to match RFC 2119) then all CVParams which are
     * part of the MzMLContent are checked to see if they are members of the
     * allowed CVMappingRule, otherwise an error will be thrown.
     * <p>
     * For other requirement levels, the existence of members of the
     * CVMappingRule is checked, otherwise an error is thrown.
     *
     * @param contentToCheck MzMLContent element to check
     * @return true if rule passes (no failures) or false otherwise
     * @throws CVMappingRuleException
     * @see com.alanmrace.jimzmlparser.mzml.MzMLContent
     * @see com.alanmrace.jimzmlparser.mzml.CVParam
     * @see RequirementLevel
     * @see     <a href="https://www.ietf.org/rfc/rfc2119.txt">RFC 2119</a>
     */
    public boolean check(MzMLContentWithParams contentToCheck) throws CVMappingRuleException {
        if (requirementLevel.equals(RequirementLevel.MAY)
                && combinationLogic.equals(CombinationLogic.OR)) {
            return checkOptionalSingle(contentToCheck);
        } else {
            return checkRequiredOrMultiple(contentToCheck);
        }
    }

    @Override
    public String toString() {
        return id + " @ \"" + cvElementPath;
    }
}
