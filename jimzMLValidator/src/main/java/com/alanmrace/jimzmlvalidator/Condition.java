/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.MzML;

/**
 *
 * @author alan.race
 */
public interface Condition {
    boolean check(MzML mzML);
}
