/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.MzML;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author alan.race
 */
public class ConditionalIf implements ConditionalRule {



    public enum CombinationLogic {
        OR,
        AND,
        XOR
    }

    CombinationLogic combinationLogic;

    List<Condition> conditions;
    List<Statement> statements;
    
    CVMapping mapping;

    public ConditionalIf(CombinationLogic combinationLogic) {
        conditions = new LinkedList<>();
        statements = new LinkedList<>();
        
        this.combinationLogic = combinationLogic;
        this.mapping = new CVMapping();
    }

    /**
     *
     * @param condition
     */
    @Override
    public void addCondition(Condition condition) {
        conditions.add(condition);
    }
    
    public Condition getCondition(int index) {
        return conditions.get(index);
    }
    
    public int getNumberConditions() {
        return conditions.size();
    }

    @Override
    public void addStatement(Statement statement) {
        statements.add(statement);
    }

    protected void setCVReferenceList(CVReferenceList referenceList) {
        this.mapping.setCVReferenceList(referenceList);
    }
    
    @Override
    public void addCVMappingRule(CVMappingRule mappingRule) {
        mapping.addCVMappingRule(mappingRule);
    }
    
    
    @Override
    public List<CVMapping> getFullMappingList() {
        List<CVMapping> mappingList = new LinkedList<>();
        
        for(Statement statement : statements) {
            if(statement instanceof IncludeMappingFileStatement)
                mappingList.add(((IncludeMappingFileStatement) statement).mapping);
        }
            
        mappingList.add(mapping);
        
        return mappingList;
    }
    
    @Override
    public List<CVMapping> getMappingList(MzML mzML) {
        boolean isCorrect = false;

        if (combinationLogic.equals(CombinationLogic.AND)) {
            isCorrect = true;
        }

        if(conditions.isEmpty())
            isCorrect = true;
        
        for (Condition condition : conditions) {
            boolean termOK = condition.check(mzML);

            switch (combinationLogic) {
                case AND:
                    isCorrect &= termOK;
                    break;
                case OR:
                    isCorrect |= termOK;
                    break;
                case XOR:
                    isCorrect ^= termOK;
                    break;
                default:
                    break;
            }
        }
        
        List<CVMapping> mappingList = new LinkedList<>();
        
        if(isCorrect) {
            for(Statement statement : statements)
                if(statement instanceof IncludeMappingFileStatement)
                    mappingList.add(((IncludeMappingFileStatement) statement).mapping);
            
            mappingList.add(mapping);
        }
        
        return mappingList;
    }
}
