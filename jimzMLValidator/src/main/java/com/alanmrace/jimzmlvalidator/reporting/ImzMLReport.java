package com.alanmrace.jimzmlvalidator.reporting;

import com.alanmrace.jimzmlparser.exceptions.InvalidXPathException;
import com.alanmrace.jimzmlparser.mzml.CVParam;
import com.alanmrace.jimzmlparser.mzml.MzML;
import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlparser.mzml.MzMLTag;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alan
 */
public class ImzMLReport {

    protected List<ReportEntry> reportEntries;

    public class ReportEntry {

        private String xPath;
        private String cvParam;

        public ReportEntry(String xPath, String cvParam) {
            this.xPath = xPath;
            this.cvParam = cvParam;
        }

        public String getXPath() {
            return xPath;
        }

        public String getCVParam() {
            return cvParam;
        }
    }

    public ImzMLReport() {
        reportEntries = new LinkedList<>();
    }

    public void addCVMappingRule(CVMappingRule rule) {
        List<CVTerm> cvTerms = rule.getCVTerms();
        
        cvTerms.forEach((term) -> {
            reportEntries.add(new ReportEntry(rule.getScopePath(), term.getTermAccession()));
        });
    }

    /**
     *
     * @param entries
     */
    public void addEntries(List<ReportEntry> entries) {
        this.reportEntries.addAll(entries);
    }
    
    public String processReport(MzML imzML) {
        String report = "";
        
        for (ReportEntry reportEntry : reportEntries) {
            LinkedList<MzMLTag> elements = new LinkedList<>();

            try {
                imzML.addElementsAtXPathToCollection(elements, reportEntry.getXPath());
                
                for (MzMLTag element : elements) {
                    if (element instanceof MzMLContentWithParams) {
                        CVParam param = ((MzMLContentWithParams) element).getCVParamOrChild(reportEntry.getCVParam());

                        if (param != null) {
                            report += param + " (" + element + ")\n";
                        }
                    }
                }
            } catch (InvalidXPathException ex) {
                Logger.getLogger(ImzMLReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return report;
    }
}
