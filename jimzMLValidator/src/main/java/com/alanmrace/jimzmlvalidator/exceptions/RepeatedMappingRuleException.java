/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;

/**
 *
 * @author Alan
 */
public class RepeatedMappingRuleException extends CVTermException {

    public RepeatedMappingRuleException(CVMappingRule mappingRule, MzMLContentWithParams content, CVTerm cvTerm) {
        super(mappingRule, content, cvTerm);
    }   

    @Override
    public String getIssueTitle() {
        return "Repetition of " + cvTerm.getTermAccession() + " used in rule " + mappingRule.getName();
    }
   
    
}
