package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;

/**
 *
 * @author Alan Race
 */
public abstract class CVTermException extends CVMappingRuleWithMzMLContentException {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 6226293163668945940L;
	protected final CVTerm cvTerm;
    
    public CVTermException(CVMappingRule mappingRule, MzMLContentWithParams content, CVTerm cvTerm) {
        super(mappingRule, content);

        if(cvTerm == null) {
            throw(new IllegalArgumentException("cvTerm cannot be null"));
        }

        this.cvTerm = cvTerm;
    }    
    
    public CVTerm getCVTerm() {
        return cvTerm;
    }
    
    @Override
    public String getIssueMessage() {
        return cvTerm.getDescription();
    }
        
    @Override
    public IssueLevel getIssueLevel() {
        switch(mappingRule.getRequirementLevel()) {
            case MAY:
                return IssueLevel.WARNING;
            case SHOULD:
                return IssueLevel.ERROR;
            case MUST:
            default:
                return IssueLevel.SEVERE;
        }
    }
}
