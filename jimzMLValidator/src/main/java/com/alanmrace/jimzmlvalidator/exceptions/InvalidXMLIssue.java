package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.exceptions.Issue;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Alan Race
 */
public class InvalidXMLIssue extends Exception implements Issue {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 5498685196113524392L;
	
	protected final Exception parentException;
    
    public InvalidXMLIssue(String issueMessage, Exception ex) {
        super(issueMessage, ex);
        
        parentException = ex;
    }

    @Override
    public String getIssueMessage() {
        return this.getLocalizedMessage();
    }

    @Override
    public String getIssueTitle() {
        String title = this.getLocalizedMessage();
        
        if(parentException instanceof SAXParseException) {
            SAXParseException ex = (SAXParseException) parentException;
            
            title = "Invalid (i)mzML at line " + ex.getLineNumber() + ", column " + ex.getColumnNumber();
        }
        
        return title;
    }

    @Override
    public IssueLevel getIssueLevel() {
        return IssueLevel.SEVERE;
    }
}
