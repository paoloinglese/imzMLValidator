package com.alanmrace.jimzmlvalidator.exceptions;

import com.alanmrace.jimzmlparser.mzml.MzMLContentWithParams;
import com.alanmrace.jimzmlvalidator.CVMappingRule;
import com.alanmrace.jimzmlvalidator.CVTerm;

/**
 *
 * @author Alan Race
 */
public class ChildUsedMappingRuleException extends CVTermException {
    
    /**
     *
     */
    private static final long serialVersionUID = 8026187027969040742L;

    public ChildUsedMappingRuleException(CVMappingRule mappingRule, MzMLContentWithParams content, CVTerm cvTerm) {
        super(mappingRule, content, cvTerm);
    }

    @Override
    public String getIssueTitle() {
        return "Child of " + cvTerm.getTermAccession() + " used in rule " + mappingRule.getName();
    }
    
}
