package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.obo.OBO;
import com.alanmrace.jimzmlvalidator.exceptions.InvalidCVMappingException;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Alan Race
 */
public class CVMappingHandler extends DefaultHandler {
    private static final Logger logger = Logger.getLogger(CVMappingHandler.class.getName());
    
    protected CVMapping cvMapping;
    
    protected CVMappingRule currentMappingRule;
    
    protected OBO obo;
    
    public void setOBO(OBO obo) {
        this.obo = obo;
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    	try {
	        switch(qName) {
	            case "CvMapping":
	                cvMapping = new CVMapping(attributes.getValue("modelName"), attributes.getValue("modelURI"), 
	                        attributes.getValue("modelVersion"));
	                
	                break;
	            case "CvReference":
	                cvMapping.addCVReference(new CVReference(attributes.getValue("cvName"), attributes.getValue("cvIdentifier")));
	                
	                break;
	            case "CvMappingRule":
	                String requirementLevelText = attributes.getValue("requirementLevel");
	                String combinationLogicText = attributes.getValue("cvTermsCombinationLogic");
	                                
	                currentMappingRule = new CVMappingRule(attributes.getValue("id"), attributes.getValue("cvElementPath"),
	                CVMappingRule.stringToRequirementLevel(requirementLevelText), attributes.getValue("scopePath"), 
	                CVMappingRule.stringToCombinationLogic(combinationLogicText));
	                
	                // optional: name
	                
	                currentMappingRule.setOBO(obo);
	                cvMapping.addCVMappingRule(currentMappingRule);
	                break;
	            case "CvTerm":
	                CVTerm cvTerm = new CVTerm(cvMapping.getCVReference(attributes.getValue("cvIdentifierRef")),
	                        attributes.getValue("termAccession"), 
	                        attributes.getValue("termName"),
	                        CVTerm.stringToBoolean(attributes.getValue("useTerm")), 
	                        CVTerm.stringToBoolean(attributes.getValue("allowChildren"))
	                        );
	                
	                String useTermName = attributes.getValue("useTermName");
	                if(useTermName != null)
	                    cvTerm.setUseTermName(CVTerm.stringToBoolean(useTermName));
	                
	                String isRepeatable = attributes.getValue("isRepeatable");
	                if(isRepeatable != null)
	                    cvTerm.setIsRepeatable(CVTerm.stringToBoolean(isRepeatable)); 
	                
	                currentMappingRule.addCVTerm(cvTerm);
	                
	                break;
	            default:
	            	logger.log(Level.FINE, "Unhandled element {0}", qName);
	            	
	            	break;
	        }
    	} catch (InvalidCVMappingException ex) {
    		logger.log(Level.SEVERE, "Invalid CV Mapping file", ex);
    	}
    }
    
    public static CVMapping parseCVMapping(InputStream cvMapping) {
        return parseCVMapping(cvMapping, null);
    }
    
    public static CVMapping parseCVMapping(InputStream cvMapping, OBO obo) {
        CVMappingHandler handler = new CVMappingHandler();
        handler.setOBO(obo);

        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();

            //parse the file and also register this class for call backs
            sp.parse(cvMapping, handler);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        return handler.getCVMapping();
    }
    
    public CVMapping getCVMapping() {
        return cvMapping;
    }
}
