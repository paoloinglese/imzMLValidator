package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.mzml.MzML;
import java.util.List;

/**
 *
 * @author Alan Race
 */
public interface ConditionalRule extends HasMappingRule {
    
    void addCondition(Condition condition);
    void addStatement(Statement statement);
    
    List<CVMapping> getMappingList(MzML mzML);
    List<CVMapping> getFullMappingList();
}
