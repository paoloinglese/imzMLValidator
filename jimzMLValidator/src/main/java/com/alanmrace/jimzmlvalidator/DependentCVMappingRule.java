/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.obo.OBOTerm;

/**
 *
 * @author Alan
 */
public class DependentCVMappingRule extends CVMappingRule {
    
    protected OBOTerm dependentTerm;
    
    public DependentCVMappingRule(OBOTerm dependentTerm, String id, String cvElementPath, RequirementLevel requirementLevel, String scopePath, CombinationLogic combinationLogic) {
        super(id, cvElementPath, requirementLevel, scopePath, combinationLogic);
        
        this.dependentTerm = dependentTerm;
    }
    
    
    public OBOTerm getDependentTerm() {
        return dependentTerm;
    }
}
