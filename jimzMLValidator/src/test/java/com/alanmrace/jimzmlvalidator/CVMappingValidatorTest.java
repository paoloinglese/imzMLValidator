/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.FatalParseException;
import com.alanmrace.jimzmlparser.imzml.ImzML;
import com.alanmrace.jimzmlparser.parser.ImzMLHandler;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alan
 */
public class CVMappingValidatorTest {
    
    private static final Logger logger = Logger.getLogger(CVMappingValidatorTest.class.getName());
    
    public CVMappingValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validate method, of class CVMappingValidator.
     * @throws com.alanmrace.jimzmlparser.exceptions.ImzMLParseException
     */
    @Test
    public void testValidate() throws FatalParseException {
        System.out.println("validate");
        ImzML imzML = ImzMLHandler.parseimzML(CVMappingValidatorTest.class.getResource(ImzMLValidatorTest.SIMPLEST_MAPPING_IMZML).getPath());
        CVMappingValidator instance = new CVMappingValidator(CVMappingHandler.parseCVMapping(CVMappingValidatorTest.class.getResourceAsStream(CVMappingHandlerTest.CVMAPPING_RESOURCE)));
        
        boolean expResult = true;
        instance.validate(imzML);
        
        //assertEquals(expResult, issues.isEmpty());
    }
    
}
