package com.alanmrace.jimzmlvalidator;

import com.alanmrace.jimzmlparser.exceptions.FatalParseException;
import com.alanmrace.jimzmlparser.imzml.ImzML;
import com.alanmrace.jimzmlparser.parser.ImzMLHandler;
import java.io.FileNotFoundException;
import java.util.List;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *  
 * 
 * @author Alan Race
 */
public class ConditionalMappingTest {
    public final static String MINREPORTING_RESOURCE = "/mapping/MSIMinimumReporting.xml";
    
    @Test
    public void testConditionalMappingTest() throws FileNotFoundException, FatalParseException {        
        ConditionalMapping conditionalMapping = ConditionalMappingHandler.parseConditionalMapping(CVMappingHandlerTest.class.getResourceAsStream(MINREPORTING_RESOURCE));
        
        System.out.println(conditionalMapping);
        assertNotNull("CV Mapping File", conditionalMapping);
        
        ImzML imzML = ImzMLHandler.parseimzML(ConditionalMappingTest.class.getResource(ImzMLValidatorTest.SIMPLEST_MAPPING_IMZML).getPath());
        
        List<CVMapping> mappingList = conditionalMapping.getMappingList(imzML);
        
        System.out.println(" -- Included Mapping Files -- ");
        for(CVMapping mapping : mappingList)
            System.out.println("Mapping included: " + mapping);
        
        assert(!mappingList.isEmpty());
    }
}
