package com.alanmrace.jimzmlvalidator;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.fail;

public class CVMappingRuleTest {

    @Test
    public void testGetXPath() {
        ImzMLValidator instance = new ImzMLValidator();



        List<CVMappingRule> rules = instance.getRulesFromXPath("/mzML/dataProcessingList/dataProcessing/processingMethod");
        if(rules.size() > 2)
            fail("Incorrect number of rules for " + "/mzML/dataProcessingList/dataProcessing/processingMethod");
    }
}
