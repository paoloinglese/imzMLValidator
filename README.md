If you find this useful please cite: 'Error-free data visualisation and processing through imzML and mzML validation' Alan Race and Andreas Römpp, Analytical Chemistry, https://pubs.acs.org/doi/10.1021/acs.analchem.8b03059

[Download the latest version](https://gitlab.com/imzML/imzMLValidator/wikis/latest)

[Read the user manual](https://gitlab.com/imzML/imzMLValidator/wikis/home)

![imzMLValidator_1_0_4k](/uploads/ce70ddef5fac357dfd86220d94eeb5f5/imzMLValidator_1_0_4k.png)